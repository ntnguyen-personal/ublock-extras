// ==UserScript==
// @name        Account Google redirect
// @include     https://example.com*
// @version     3
// @updateURL   https://gitlab.com/ntnguyen-personal/ublock-extras/-/raw/main/account_google_redirect.js
// ==/UserScript==

function embed_url(url) {
  let item = document.querySelector("body > div");
  let href_embed = document.createElement("a");
  href_embed.target = href_embed.href = url;
  href_embed.appendChild(document.createTextNode(url));
  item.appendChild(href_embed);
}

let url = location.hash.slice(1);
url = decodeURIComponent(url.replaceAll("25", ""));
url = new URL(url.split("?")[0].split("&")[0]);

if (url.hostname === "t.co") {
  embed_url(url.href);
  embed_url("https://nitter.net/" + url.split("https://")[0]);
}
else if (
  url.hostname.endsWith("twitter.com") ||
  url.hostname.endsWith("reddit.com") ||
  (url.hostname === "www.pixiv.net")
  )
  embed_url(url.href);
